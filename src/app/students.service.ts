import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import {enableProdMode} from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  studentsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection(`students`);
  private URL = "https://ri3qwl6g5i.execute-api.us-east-1.amazonaws.com/examA-evgeni";


  constructor(private db: AngularFirestore, private http:HttpClient) { }

    getStudents(): Observable<any[]> {
    this.studentsCollection = this.db.collection(`students`)
    return this.studentsCollection.snapshotChanges();    
  }

  addStudent(math:number, met:number, payed:boolean){
    const student  = {math:math, met:met, payed:payed} 
    this.db.collection(`students`).add(student);
  }

  deleteStudent(id:string ){
    this.db.doc(`students/${id}`).delete();
  
  }

  predict(math:number, met:number, payed:boolean){
    
    let json = {
      "math": math,
      "met": met,
      "payed": payed

    }
 
     let body = JSON.stringify(json);
     return this.http.post<any>(this.URL, body).pipe(map(res =>{
       console.log(res);
       let final:string = res.result;
       console.log(final);
       /*final  = final.replace('[',"");
       final  = final.replace(']',"");
       console.log(final);*/
 
       return final;
 
     })
     )
  
   
  }

  updateResult(id:string, result){
    this.db.doc(`students/${id}`).update(
      {
        result:result,
        saved:true,

     

      }
    )
  }
}
