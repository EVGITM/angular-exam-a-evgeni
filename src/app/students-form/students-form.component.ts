import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Student } from '../interfaces/students';

@Component({
  selector: 'students-form',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

  @Input() math:number;
  @Input() met:number;
  @Input() payed;

  @Input() id:string; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>();

  @Output() closeEdit = new EventEmitter<null>();
  errormassage:string ="";
  payedarr:Array<boolean>  = [true,false];


  updateParent(){
    if (this.math<0 || this.math> 100) {
      this.errormassage = "math grade must be between 0 to 100"

    }
    if (this.met<0 || this.met> 800) {
      this.errormassage = "Psychometric grade must be between 0 to 800"

    }else{
      console.log(this.payedarr)

      console.log(this.payed)
    let student:Student = {id:this.id, math:this.math, met:this.met, payed:this.payed};
    this.update.emit(student)
    if (this.formType == "Add Sudent"){
      this.math = null;
      this.met = null;
      this.payed = null;
    }
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }
  constructor() { }

  ngOnInit(): void {
  }

}
