import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string;
  hasError:boolean = false;
  errorMassage:string;

  onSubmit(){
    if(this.password.length < 8){
      this.hasError=true;
      this.errorMassage = 'The password must contain at least 8 characters';
    }else{
    this.Auth.signup(this.email, this.password).then(res => {
      console.log('Sign Up Successful');
      this.router.navigate(['/welcome']); 
    }).catch(() => {
      this.hasError=true;
      this.errorMassage = 'This email is already in use by another account';
    });
  }
  }

  constructor(public Auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
