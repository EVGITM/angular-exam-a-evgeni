import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/students';
import { StudentsService } from '../students.service';



@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  displayedColumns: string[] = ['position', 'email', 'math', 'met', 'payed', 'delete','predict','result'];
  userId:string; 
  students$;
  students:Student[];
  addStudentsFormOPen:boolean = false;
  editState:Array <boolean> = [];
  tempres:number;
  resultes:Array <string> = ['will drop','will not drop' ];
  payedarr:Array<boolean>  = [true,false];



  constructor(private StudentsService:StudentsService, public Auth:AuthService) { }

  //functions
  
  add(student:Student){
    this.StudentsService.addStudent(student.math, student.met, student.payed);

  }

  deleteStudent(id:string){
    this.StudentsService.deleteStudent(id);
  }

  predict(math:number, met:number, payed:boolean, index:number){
    this.StudentsService.predict(math,met, payed).subscribe(
      (res) => {
        this.tempres = Number(res)
        if(Number(res)>0.5){
          this.students[index].result = 'will not drop';  
        }else{
        this.students[index].result = 'will drop';  

        }
        
        console.log(this.students[index].result);


      }
    )

  }

  
  updateResult(id:string, result, index){
    console.log(id);
    this.StudentsService.updateResult(id, result);
    this.students[index].saved = true;  
  }

  ngOnInit(): void {
  
    this.Auth.getUser().subscribe(
      (user) =>{
        this.userId = user.uid;
        console.log(this.userId);
          this.students$ = this.StudentsService.getStudents();
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const student:Student = document.payload.doc.data();
                student.id = document.payload.doc.id;
                student.email = user.email;

                   this.students.push(student); 
                   console.log(student)
              }                        
            })
           
     }
   )
 
 }

}
